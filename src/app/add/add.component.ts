import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DatatranferService } from '../config/datatranfer.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  submitted = false;
  constructor(private datatransSRV:DatatranferService) { 
    
  }
  ngOnInit(){}
  sendMessage(message:any) {
    this.datatransSRV.sendMessage(message);
  }

}
