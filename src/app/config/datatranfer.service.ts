import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DatatranferService {

  private subject = new Subject<any>();

  constructor() {}

  sendMessage(data: any) {
    this.subject.next(data);
  }

  receivedMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
