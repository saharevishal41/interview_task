import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DatatranferService } from '../config/datatranfer.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  message: string="";
  subs!: Subscription;

  constructor(private datatransSRV:DatatranferService,) {
   
  }

  ngOnInit(){
    this.subs = this.datatransSRV.receivedMessage().subscribe((d) => {
      this.message = d;
    });
  }
  ngOnDestroy(){
    this.subs.unsubscribe();
  }
}
